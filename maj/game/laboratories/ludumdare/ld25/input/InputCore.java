package maj.game.laboratories.ludumdare.ld25.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import maj.game.laboratories.ludumdare.ld25.input.InputCore.KeyMap.Keyling;

/**
 *
 * @author Michcioperz <michcioperz@gmail.com>
 */
public class InputCore implements KeyListener {
    
    Map<Integer, Keyling> map = new HashMap<>();

    @Override
    public void keyPressed(KeyEvent e) {
        map.get(e.getKeyCode()).press();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        map.get(e.getKeyCode()).release();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }
    
    public KeyMap keymap;
    
    public InputCore() {
        keymap = new KeyMap();
        map.put(KeyEvent.VK_UP, keymap.up);
        map.put(KeyEvent.VK_DOWN, keymap.down);
        map.put(KeyEvent.VK_LEFT, keymap.left);
        map.put(KeyEvent.VK_RIGHT, keymap.right);
        map.put(KeyEvent.VK_W, keymap.up);
        map.put(KeyEvent.VK_S, keymap.down);
        map.put(KeyEvent.VK_A, keymap.left);
        map.put(KeyEvent.VK_D, keymap.right);
    }
    
    public class KeyMap {
        List<Keyling> keymap = new ArrayList<>();
        
        public Keyling up = new Keyling();
        public Keyling down = new Keyling();
        public Keyling left = new Keyling();
        public Keyling right = new Keyling();
    
        public final class Keyling {
            public boolean state0 = false;
            public boolean state1 = false;
            public boolean state2 = false;
            public Keyling() {
                keymap.add(this);
            }
            public void update() {
                state0 = state1;
                state1 = state2;
            }
            public boolean justPressed() {
                return !state0 && state1;
            }
            public boolean justReleased() {
                return state0 && !state1;
            }
            public void press() {
                state2 = true;
            }
            public void release() {
                state2 = false;
            }
        }
        
        public void cleanupState2() {
            for (Keyling keyla: keymap) {
                keyla.release();
            }
        }
    }
}
