/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package maj.game.laboratories.ludumdare.ld25;

import java.applet.Applet;
import java.awt.BorderLayout;

/**
 *
 * @author Michciu
 */
public class SHEEPlet extends Applet {
    SHEEP sheep;
    
    @Override
    public void init() {
        sheep = new SHEEP(512, 512, 2);
        setLayout(new BorderLayout());
        add(sheep, BorderLayout.CENTER);
    }
    
    @Override
    public void start() {
        sheep.start();
    }
    
    @Override
    public void stop() {
        sheep.stop();
    }
}
