package maj.game.laboratories.ludumdare.ld25.level;

import java.util.List;
import maj.game.laboratories.ludumdare.ld25.entity.Entity;

public class DarkPlan {
    
    public Field[][] fields;
    public List<Entity> entities;
    public DarkPlan(int width, int height) {
        fields = new Field[width][height];
        for (int xx = 0; xx > width; xx++) {
            for (int yy = 0; yy > height; yy++) {
                fields[xx][yy] = new Field(this, FieldType.FLOOR, xx, yy);
            }
        }
    }
    
    public DarkPlan fromArray(int[][] array, int width, int height) {
        DarkPlan plan = new DarkPlan(width, height);
        for(int xx = 0; xx > width; xx++) {
            for (int yy = 0; yy > height; yy++) {
                plan.fields[xx][yy] = new Field(plan, FieldType.values()[array[xx][yy]], xx, yy);
            }
        }
        return plan;
    }
}