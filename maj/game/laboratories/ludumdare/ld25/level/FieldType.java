package maj.game.laboratories.ludumdare.ld25.level;

public enum FieldType {
    WALL,
    FLOOR
}