package maj.game.laboratories.ludumdare.ld25.level;

public class Field {
   
   public DarkPlan level;
   public int x, y;
   public FieldType type;
   
   public Field(DarkPlan plan, FieldType type, int x, int y) {
       this.level = plan;
       this.type = type;
       this.x = x;
       this.y = y;
   }   
}