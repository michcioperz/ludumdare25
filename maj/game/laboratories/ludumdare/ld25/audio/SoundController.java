package maj.game.laboratories.ludumdare.ld25.audio;

import paulscode.sound.SoundSystem;
import paulscode.sound.SoundSystemConfig;
import paulscode.sound.SoundSystemException;
import paulscode.sound.codecs.CodecWav;
import paulscode.sound.codecs.CodecJOrbis;
import paulscode.sound.libraries.LibraryJavaSound;

/**
 *
 * @author Michcioperz <michcioperz@gmail.com>
 */
public class SoundController {
    SoundSystem system;
    
    public SoundController() {
        try {
            SoundSystemConfig.addLibrary(LibraryJavaSound.class);
            SoundSystemConfig.setCodec("wav", CodecWav.class);
            SoundSystemConfig.setCodec("ogg", CodecJOrbis.class);
            system = new SoundSystem();
        } catch (SoundSystemException ex) {
            SoundSystemConfig.getLogger().printStackTrace(ex, 2);
            system = null;
        }
    }
    
    public void playBackground() {
        // TODO:
    }
    
    public void stopBackground() {
        // TODO:
    }
    
    public void kill() {
        // TODO:
    }
    
    public SoundController init() {
        return new SoundController();
    }
}
