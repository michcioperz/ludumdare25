package maj.game.laboratories.ludumdare.ld25.entity;

import maj.game.laboratories.ludumdare.ld25.level.DarkPlan;

public abstract class Entity {
    public DarkPlan level;
    public int direction;
}