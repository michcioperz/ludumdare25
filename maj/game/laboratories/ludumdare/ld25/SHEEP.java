package maj.game.laboratories.ludumdare.ld25;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;
import maj.game.laboratories.ludumdare.ld25.input.InputCore;

/**
 *
 * @author Michcioperz <michcioperz@gmail.com>
 */
public class SHEEP extends Canvas implements Runnable {
    int width, height, scale;
    public InputCore inputManager;
    
    public SHEEP(int width, int height, int scale) {
        this.width = width;
        this.height = height;
        this.scale = scale;
        this.setPreferredSize(new Dimension(width * scale, height * scale));
        this.setMinimumSize(new Dimension(width * scale, height * scale));
        this.setMaximumSize(new Dimension(width * scale, height * scale));
        inputManager = new InputCore();
        this.addKeyListener(inputManager);
    }
    
    public static void main(String[] args) {
        SHEEP sheep;
        int theScale = 2;
        if (args.length >= 1) {
            theScale = Integer.parseInt(args[0]);
        }
        sheep = new SHEEP(512, 512, theScale);
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(sheep);
        JFrame frame = new JFrame();
        frame.setContentPane(panel);
        frame.setTitle("Michcioperz's Awesome Ludum Dare 25 Game. Tell me if any isn't true.");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
        sheep.start();
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    

    public void start() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void stop() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
